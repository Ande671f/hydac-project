﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sensor_Generator
{
    abstract class LoginDatabase
    {
        static string ServerNavn { get; set; } = "10.56.8.32";
        static string DatabaseNavn { get; set; } = "A_GRUPEDB10_2019";
        static string BrugerID { get; set; } = "A_GRUPE10";
        static string Adgangskode { get; set; } = "A_OPENDB10";
        public static string LoginStreng { get; set; } = $"Server={ServerNavn};Database={DatabaseNavn};User ID={BrugerID};Password={Adgangskode}";
    }
}
