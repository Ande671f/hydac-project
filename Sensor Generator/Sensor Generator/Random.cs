﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading;

namespace Sensor_Generator
{
    class Random
    {
        int Id = 1;
        bool Stadie = true;
        public void RandomGen()
        {
            int mArangeMin = 4;
            int mArangeMax = 20;


            System.Random random = new System.Random();
            int startRandom = random.Next(mArangeMin, mArangeMax);

            int randomSensorReading = startRandom;
            // int count = 0;
            while (true)
            {
                // count++;
                if (randomSensorReading == 20)
                {
                    randomSensorReading = random.Next(randomSensorReading - 1, mArangeMax);
                }
                else if (randomSensorReading == 4)
                {
                    randomSensorReading = random.Next(mArangeMin, randomSensorReading + 2);
                }
                else
                {
                    randomSensorReading = random.Next(randomSensorReading - 1, randomSensorReading + 2);
                }

                TilføjTilDatabase(randomSensorReading);
                Thread.Sleep(1000);
        }

            
    }
        public void TilføjTilDatabase(int sensorreading)
        {
            using (SqlConnection connect = new SqlConnection(LoginDatabase.LoginStreng))
            {
                string query = "INSERT INTO Sensor VALUES (@Stadie, @mA, @Tid, @SensorID)";
                SqlCommand command = new SqlCommand(query, connect);
                command.Parameters.AddWithValue("@Stadie", Stadie);
                command.Parameters.AddWithValue("@mA", sensorreading);
                command.Parameters.AddWithValue("@Tid", DateTime.Now);
                command.Parameters.AddWithValue("@SensorID", Id);

                connect.Open();
                int result = command.ExecuteNonQuery();
                if (result < 0)
                {
                    Console.WriteLine("Error");
                    Console.ReadLine();
                }
                connect.Close();
            }

        }
        }
    }
