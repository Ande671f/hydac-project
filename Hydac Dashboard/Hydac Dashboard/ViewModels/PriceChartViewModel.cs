﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using LiveCharts;
using LiveCharts.Wpf;
using System.Threading;
using System.ComponentModel;
using LiveCharts.Configurations;
using Hydac_Dashboard.Models.ChartModels;
using Hydac_Dashboard.Models;
using System.Threading.Tasks;
using System.Globalization;

namespace Hydac_Dashboard.ViewModels
{
    public class PriceChartViewModel : INotifyPropertyChanged
    {
        private double _axisMax;
        private double _axisMin;
        Hydac_Dashboard.Models.Component component;
        

        public ChartValues<PriceChartData> ChartValues { get; set; }
        public Func<double, string> DateTimeFormatter { get; set; }
        public Func<double, string> ExpenditureFormatter { get; set; }
        public double AxisStep { get; set; }
        public double AxisUnit { get; set; }

        public bool IsReading { get; set; }
        private double currentExpenditure;
        public double CurrentExpenditure 
        { 
            get => currentExpenditure;
            set
            {
                currentExpenditure = value;
                OnPropertyChanged("CurrentExpenditure");
            }
        }

        public double AxisMax
        {
            get { return _axisMax; }
            set
            {
                _axisMax = value;
                OnPropertyChanged("AxisMax");
            }
        }
        public double AxisMin
        {
            get { return _axisMin; }
            set
            {
                _axisMin = value;
                OnPropertyChanged("AxisMin");
            }
        }

        public PriceChartViewModel()
        {
            //To handle live data easily, in this case we built a specialized type
            //the MeasureModel class, it only contains 2 properties
            //DateTime and Value
            //We need to configure LiveCharts to handle MeasureModel class
            //The next code configures MeasureModel  globally, this means
            //that LiveCharts learns to plot MeasureModel and will use this config every time
            //a IChartValues instance uses this type.
            //this code ideally should only run once
            //you can configure series in many ways, learn more at 
            //http://lvcharts.net/App/examples/v1/wpf/Types%20and%20Configuration
            component = DatabaseHelper.RetrieveComponentData()[0];
            var mapper = Mappers.Xy<PriceChartData>()
                .X(model => model.Time.Ticks)   //use DateTime.Ticks as X
                .Y(model => model.Expenditure);           //use the value property as Y

            //lets save the mapper globally.
            Charting.For<PriceChartData>(mapper);

            //the values property will store our values array
            ChartValues = new ChartValues<PriceChartData>();

            //lets set how to display the X Labels
            DateTimeFormatter = value => new DateTime((long)value).ToString("HH:mm:ss");
            ExpenditureFormatter = value => Math.Round(value, 2).ToString();

            //AxisStep forces the distance between each separator in the X axis
            AxisStep = TimeSpan.FromSeconds(20).Ticks;
            //AxisUnit forces lets the axis know that we are plotting seconds
            //this is not always necessary, but it can prevent wrong labeling
            AxisUnit = TimeSpan.TicksPerSecond;

            SetAxisLimits(DateTime.Now);

            //The next code simulates data changes every 300 ms

            IsReading = true;


        }

        public void GetLostData()
        {
            int missedSensorIndex = 0;
            var tuple = DatabaseHelper.RetrieveLostData();
            double InitialExpenditure = tuple.Item1;
            List<Sensor> missedSensors = DatabaseHelper.RetrieveSensorPostDate(tuple.Item2);
            Sensor currentMissedSensor = missedSensors[0];
            while (currentMissedSensor != missedSensors[missedSensors.Count - 1])
            {
                component.LastSensorRead = currentMissedSensor;
                if (ChartValues.Count != 0)
                {
                    ChartValues.Add(new PriceChartData(component, ChartValues[ChartValues.Count - 1].Expenditure));
                }
                else
                    ChartValues.Add(new PriceChartData(component, InitialExpenditure));
                missedSensorIndex++;
                if (missedSensorIndex <= missedSensors.Count - 1)
                {
                    currentMissedSensor = missedSensors[missedSensorIndex];
                }


                //lets only use the last 150 values
                if (ChartValues.Count > 150)
                    ChartValues.RemoveAt(0);
            }

        }

        public void StartPriceChart()
        {
            GetLostData();
            IsReading = true;
            Task.Factory.StartNew(Read);
        }

        public void StopPriceChart()
        {
            IsReading = false;
            var temp = ChartValues[ChartValues.Count - 1];
            DatabaseHelper.UpdateHistory(temp.Expenditure, temp.Time);
        }

        private void Read()
        {
            while (IsReading)
            {
                var now = DateTime.Now;
                component.UpdateSensor();
                if (ChartValues.Count != 0)
                {
                    ChartValues.Add(new PriceChartData(component, ChartValues[ChartValues.Count - 1].Expenditure));
                }
                else
                    ChartValues.Add(new PriceChartData(component, 0));
                CurrentExpenditure = Math.Round(ChartValues[ChartValues.Count - 1].Expenditure, 2);
                SetAxisLimits(now);

                //lets only use the last 150 values
                if (ChartValues.Count > 150)
                    ChartValues.RemoveAt(0);
                Thread.Sleep(1000);
            }
        }


        private void SetAxisLimits(DateTime now)
        {
            AxisMax = now.Ticks + TimeSpan.FromSeconds(1).Ticks; // lets force the axis to be 1 second ahead
            AxisMin = now.Ticks - TimeSpan.FromSeconds(100).Ticks; // and 8 seconds behind
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
