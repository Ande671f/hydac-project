﻿using Hydac_Dashboard.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Hydac_Dashboard.ViewModels
{
    public class MainViewModel
    {
        public Hydac_Dashboard.Models.Component component;

        public MainViewModel()
        {
            component = DatabaseHelper.RetrieveComponentData()[0];
        }

        void UpdateGraph()
        {
                var temp = DatabaseHelper.RetrieveLatestPumpData(component.LastSensorRead.Id);
                component.LastSensorRead = temp[temp.Count - 1];
        }

    }
}
