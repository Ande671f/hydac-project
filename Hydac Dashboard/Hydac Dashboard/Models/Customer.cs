﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hydac_Dashboard
{
    public class Customer
    {
        public int Id { get; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Phonenumber { get; set; }
        
        public string Adresse { get; set; }

        public Customer(int id, string email, string name, string phonenumber, string adresse) 
        {
            Id = id;
            Email = email;
            Name = name;
            Phonenumber = phonenumber;
            Adresse = adresse;

        }

        public override string ToString()
        {
            return $"{Id}, {Email}, {Name}, {Phonenumber}, {Adresse}";
        }
    }


}
