﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hydac_Dashboard.Models
{
    public enum SubscriptionType
    {
        Standard,
        Capped
    }

    public class Subscription
    {
        public SubscriptionType Type { get; private set; }
        public double Price { get; private set; }

        public Subscription(SubscriptionType type, double price)
        {
            Type = type;
            Price = price;
        }

        public override string ToString()
        {
            return $"{Type}, {Price}";
        }
    }

    
}
