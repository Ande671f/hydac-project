﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hydac_Dashboard.Models
{
    public static class Converter
    {
        public static double Convert(Component component)
        {
            //Fastlagt interval for sensor aflæsning
            double mAmax = 20;
            double mAmin = 4;

            double maxFlow = component.Flowrate;
            double minFlow = 0;

            double sensorReading = component.LastSensorRead.mA;

            double price = component.subscription.Price;


            //Konverter maxFlow og sensorReading fra Liter/minut til Liter/sekund
            maxFlow = maxFlow / 60;

            //Find nuværende procent tal for sensoren i forhold til minimum og maksimum
            double SensorPercentage = (sensorReading - mAmin) / (mAmax - mAmin);

            //Procent tal omregnes i forhold til det nuværende flow (spænd mellem maxFlow og minFlow)
            double currentLiterPerSecond = ((maxFlow - minFlow) * SensorPercentage) + minFlow;

            return currentLiterPerSecond * price;
        }
    }
}
