﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace Hydac_Dashboard.Models
{
    public static class DatabaseHelper
    {
        static string ServerName { get; set; } = "10.56.8.32";
        static string DatebaseName { get; set; } = "A_GRUPEDB10_2019";
        static string UserId { get; set; } = "A_GRUPE10";
        static string Password { get; set; } = "A_OPENDB10";
        public static string LoginString { get; set; } = $"Server={ServerName};Database={DatebaseName};User ID={UserId};Password={Password}";
        public static string RetrieveData()
        {
            return RetrieveComponentData()[0].subscription.Type.ToString();
        }

        public static List<Sensor> RetrieveLatestPumpData(int latestMeasureId)
        {
            List<Sensor> Sensors = new List<Sensor>();
            using (SqlConnection connect = new SqlConnection(LoginString))
            {
                string query = $"SELECT * FROM Sensor WHERE MåleId > {latestMeasureId}; ";
                SqlCommand command = new SqlCommand(query, connect);
                connect.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = 0;
                        SensorState state = SensorState.On;
                        double mA = 0;
                        DateTime timeStamp = DateTime.Now;
                        int measureId = 0;

                        id = (int)reader["SensorId"];
                        state = (SensorState)((int)reader["Stadie"]);
                        mA = (double)reader["mA"];
                        timeStamp = (DateTime)reader["Tid"];
                        measureId = (int)reader["MåleId"];
                        Sensors.Add(new Sensor(id, measureId, state, mA, timeStamp));

                    }
                }

            }
            return Sensors;
        }

        public static string RetrieveSubscription()
        {
            return RetrieveSubscriptionData()[0].ToString();
        }

        public static List<Subscription> RetrieveSubscriptionData()
        {
            List<Subscription> subscriptions = new List<Subscription>();
            using (SqlConnection connect = new SqlConnection(LoginString))
            {
                string query = $"SELECT * FROM Abonnement";
                SqlCommand command = new SqlCommand(query, connect);
                connect.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        SubscriptionType subscriptionType = SubscriptionType.Standard;
                        double price = 0;


                        subscriptionType = (SubscriptionType)(int)reader["AbonnementType"];
                        price = (double)reader["Pris"];

                        subscriptions.Add(new Subscription(subscriptionType, price));
                    }
                }
            }
            return subscriptions;
        }
        public static List<Customer> RetrieveCustomersData()
        {
            List<Customer> customers = new List<Customer>();
            using (SqlConnection connect = new SqlConnection(LoginString))
            {
                string query = $"SELECT * FROM Kunde; ";
                SqlCommand command = new SqlCommand(query, connect);
                connect.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = 0;
                        string email = "";
                        string name = "";
                        string phonenumber = "";
                        string adress = "";

                        id = (int)reader["KundeID"];
                        email =((string)reader["Email"]);
                        name = (string)reader["Navn"];
                        phonenumber = (string)reader["Telefon"];
                        adress = (string)reader["Adresse"];
                        customers.Add(new Customer(id, email, name, phonenumber, adress));

                    }
                }

            }
            return customers;
        }
        public static List<Component> RetrieveComponentData()
        {
            List<Component> components = new List<Component>();
            using (SqlConnection connect = new SqlConnection(LoginString))
            {
                string query = "SELECT * FROM Komponent INNER JOIN Kunde ON Komponent.KundeID = Kunde.KundeID INNER JOIN Sensor ON Komponent.MåleID = Sensor.MåleID;";
                SqlCommand command = new SqlCommand(query, connect);
                connect.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        //Component variables
                        int id;
                        Customer customer;
                        ComponentName componentname;
                        id = (int)reader["KomponentID"];
                        componentname = (ComponentName)((int)reader["EnumIndeks"]);

                        //Customer variables
                        int cId = 0;
                        string email = "";
                        string name = "";
                        string phonenumber = "";
                        string adress = "";
                        cId = (int)reader["KundeID"];
                        email = ((string)reader["Email"]);
                        name = (string)reader["Navn"];
                        phonenumber = (string)reader["Telefon"];
                        adress = (string)reader["Adresse"];

                        //Subscription
                        Subscription subscription = null;
                        int subId = (int)reader["AbonnementType"];
                        List<Subscription> subs = RetrieveSubscriptionData();
                        foreach(Subscription s in subs)
                        {
                            if ((int)s.Type == subId)
                            {
                                subscription = s;
                            }
                        }

                        customer = new Customer(cId, email, name, phonenumber, adress);
                        //TODO: Change this hack
                        var allPumpData = RetrieveLatestPumpData(0);

                        components.Add(new Component(id, customer, subscription, allPumpData[allPumpData.Count - 1], componentname)); 
                    }
                }

            }
            return components;
        }

        public static void UpdateHistory(double price, DateTime timestamp)
        {
            List<Sensor> Sensors = new List<Sensor>();
            using (SqlConnection connect = new SqlConnection(LoginString))
            {
                string query = $"UPDATE Historik SET " +
                    $"totalPris = {price.ToString("#.##").Replace(',', '.')}, tid = '{timestamp}';";
                SqlCommand command = new SqlCommand(query, connect);
                connect.Open();
                command.ExecuteReader();
            }
            
        }

        public static Tuple<double,DateTime> RetrieveLostData()
        {
            double pris = 0;
            DateTime datetime = DateTime.Now;
            List<Sensor> Sensors = new List<Sensor>();
            using (SqlConnection connect = new SqlConnection(LoginString))
            {
                string query = $"SELECT * FROM Historik";
                SqlCommand command = new SqlCommand(query, connect);
               
                connect.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        pris = (double)reader["totalPris"];
                        datetime = (DateTime)reader["tid"];
                       
                    }
                }

            }
            return new Tuple<double, DateTime>(pris, datetime);
        }
        public static List<Sensor> RetrieveSensorPostDate(DateTime datetime)
        {
            List<Sensor> Sensors = new List<Sensor>();
            using (SqlConnection connect = new SqlConnection(LoginString))
            {
                string query = $"SELECT * FROM Sensor WHERE Tid > '{datetime}';";
                SqlCommand command = new SqlCommand(query, connect);
                connect.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = 0;
                        SensorState state = SensorState.On;
                        double mA = 0;
                        DateTime timeStamp = DateTime.Now;
                        int measureId = 0;

                        id = (int)reader["SensorId"];
                        state = (SensorState)((int)reader["Stadie"]);
                        mA = (double)reader["mA"];
                        timeStamp = (DateTime)reader["Tid"];
                        measureId = (int)reader["MåleId"];
                        Sensors.Add(new Sensor(id, measureId, state, mA, timeStamp));

                    }
                }

            }
            return Sensors;
        }

    }
}
