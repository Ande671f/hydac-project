﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hydac_Dashboard.Models.ChartModels
{
    public class PriceChartData
    {
        public DateTime Time { get; set; }
        double lastPrice;
        double currentPrice;
        public double Expenditure { get { return currentPrice + lastPrice; } }
        public PriceChartData(Component component, double lastPrice)
        {
            this.lastPrice = lastPrice;
            Time = component.LastSensorRead.TimeStamp;
            currentPrice = component.Expenditure;
        }
    }
}
