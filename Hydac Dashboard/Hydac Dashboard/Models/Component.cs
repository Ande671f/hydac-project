﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hydac_Dashboard.Models
{
    public enum ComponentName
    {
        DVA_Kit_Type_16_70,
        DVA_Kit_Type_26_70,
        DVA_Kit_Type_36_70,
        DVA_KIT_TYPE_45_70,
        DVA_Kit_Type_11_140,
        DVA_Kit_Type_16_140,
        DVA_Kit_Type_20_140,
        DVA_Kit_Type_35_140
    }
    public class Component
    {
        public int Id { get;}

        public Customer customer { get; set; }

        public Subscription subscription { get; set; }

        public Sensor LastSensorRead { get; set; }

        public double Expenditure { get { return Converter.Convert(this); } }

        public ComponentName ComponentType { get; }
        public double Flowrate { get; }

        public Component(int id, Customer customer, Subscription subscription, Sensor lastsensorread, ComponentName componenttype)
        {
            Id = id;
            this.customer = customer;
            this.subscription = subscription;
            LastSensorRead = lastsensorread;
            ComponentType = componenttype;

            switch (ComponentType)
            {
                case ComponentName.DVA_Kit_Type_16_70:
                    Flowrate = 16;
                    break;
                case ComponentName.DVA_Kit_Type_26_70:
                    Flowrate = 26;
                    break;
                case ComponentName.DVA_Kit_Type_36_70:
                    Flowrate = 36;
                    break;
                case ComponentName.DVA_KIT_TYPE_45_70:
                    Flowrate = 45;
                    break;
                case ComponentName.DVA_Kit_Type_11_140:
                    Flowrate = 11;
                    break;
                case ComponentName.DVA_Kit_Type_16_140:
                    Flowrate = 16;
                    break;
                case ComponentName.DVA_Kit_Type_20_140:
                    Flowrate = 20;
                    break;
                case ComponentName.DVA_Kit_Type_35_140:
                    Flowrate = 35;
                    break;
                default:
                    break;
            }
        }

        public void UpdateSensor()
        {
            var temp = DatabaseHelper.RetrieveLatestPumpData(LastSensorRead.Id);
            LastSensorRead = temp[temp.Count - 1];
        }



    }
}
