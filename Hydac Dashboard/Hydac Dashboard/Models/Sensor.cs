﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hydac_Dashboard.Models
{
    public enum SensorState
    {
        Off,
        On,
        Error
    }
    public class Sensor
    {
        public int Id { get; private set; }
        public int MeasureId { get; private set; }
        public SensorState State { get; private set; }
        public double mA { get; private set; }
        public DateTime TimeStamp { get; private set; }

        public Sensor(int id, int measureId, SensorState state, double mA, DateTime timeStamp)
        {
            Id = id;
            MeasureId = measureId;
            State = state;
            this.mA = mA;
            TimeStamp = timeStamp;
        }

        public override string ToString()
        {
            return $"{Id}, {MeasureId}, {State}, {mA}, {TimeStamp}";
        }
    }
}
