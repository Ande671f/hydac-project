﻿using Hydac_Dashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using System.Threading;
using System.ComponentModel;
using LiveCharts.Configurations;
using Hydac_Dashboard.Models.ChartModels;
using Hydac_Dashboard.ViewModels;

namespace Hydac_Dashboard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PriceChartViewModel viewModel = new PriceChartViewModel();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = viewModel;
            
        }

        private void StartPriceChart_Btn_Click(object sender, RoutedEventArgs e)
        {
            viewModel.StartPriceChart();
        }

        private void StopPriceChart_Btn_Click(object sender, RoutedEventArgs e)
        {
            viewModel.StopPriceChart();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            var temp = viewModel.ChartValues[viewModel.ChartValues.Count - 1];
            DatabaseHelper.UpdateHistory(temp.Expenditure, temp.Time);
        }
    }
}
